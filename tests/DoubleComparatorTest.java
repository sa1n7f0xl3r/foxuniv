package tests;

import util.DoubleComparator;

public class DoubleComparatorTest
{
    private static boolean isEqual(int first, int second)
    {
        return first == second;
    }

    public static void main(String[] args)
    {
        DoubleComparator comparator = new DoubleComparator();

        System.out.println(isEqual(comparator.compare(3.4, 4.2), -1));
        System.out.println(isEqual(comparator.compare(5.6, 2.1), 1));
        System.out.println(isEqual(comparator.compare(1.2, 1.2), 0));
    }
}