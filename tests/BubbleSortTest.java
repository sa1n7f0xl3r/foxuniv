package tests;

import util.BubbleSort;
import util.IntegerComparator;
import util.SingleLinkedList;
import util.SingleLinkedListIterator;

public class BubbleSortTest
{
    public static void main(String[] args)
    {
        SingleLinkedList list = new SingleLinkedList();

        list.add(4);
        list.add(1);
        list.add(10);
        list.add(2);
        list.add(3);
        list.add(0);
        list.add(3);

        SingleLinkedListIterator iterator = new SingleLinkedListIterator(list);

        for (iterator.first(); !iterator.isDone(); iterator.next()) System.out.print(iterator.current() + " ");
        System.out.println();

        BubbleSort bubbleSort = new BubbleSort(new IntegerComparator());
        bubbleSort.sort(list);

        for (iterator.first(); !iterator.isDone(); iterator.next()) System.out.print(iterator.current() + " ");
        System.out.println();
    }
}