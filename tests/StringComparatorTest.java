package tests;

import util.StringComparator;

public class StringComparatorTest
{
    private static boolean isEqual(int first, int second)
    {
        return first == second;
    }

    public static void main(String[] args)
    {
        StringComparator comparator = new StringComparator();

        System.out.println(isEqual(comparator.compare("test", "testtt"), -1));
        System.out.println(isEqual(comparator.compare("tesssst", "test"), 1));
        System.out.println(isEqual(comparator.compare("test", "test"), 0));
    }
}