package tests;

import util.SingleLinkedList;
import util.SingleLinkedListIterator;

public class SingleLinkedListIteratorTest
{
    public static void main(String[] args)
    {
        SingleLinkedList list = new SingleLinkedList();

        list.add("test");
        list.add("second");
        list.add(3);
        list.add(523.3);
        list.add('a');

        SingleLinkedListIterator iterator = new SingleLinkedListIterator(list);

        for (iterator.first(); !iterator.isDone(); iterator.next()) System.out.println(iterator.current());

        System.out.println();

        for (iterator.last(); !iterator.isDone(); iterator.previous()) System.out.println(iterator.current());
    }
}