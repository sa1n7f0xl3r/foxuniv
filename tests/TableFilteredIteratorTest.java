package tests;

import util.FilteredIterator;
import util.TableIterator;
import util.interfaces.Predicate;

public class TableFilteredIteratorTest
{
    private static class Point
    {
        private final double x;
        private final double y;

        public Point(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        public double distanceBetweenPoints()
        {
            return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
        }

        public String toString()
        {
            return String.format("(%.3f, %.3f)", x, y);
        }
    }

    private static class PointsCloserThanPredicate implements Predicate
    {
        private final double maxDistance;

        public PointsCloserThanPredicate(double maxDistance)
        {
            this.maxDistance = maxDistance;
        }

        public boolean accept(Object object)
        {
            return ((Point)object).distanceBetweenPoints() < maxDistance;
        }
    }

    public static void main(String[] args)
    {
        Point A = new Point(2.5, 3.4);
        Point B = new Point(5.3, 3.1);
        Point C = new Point(30.2, 10.2);
        Point D = new Point(2.1, 3.2);
        Point E = new Point(19.6, 12.3);

        Point[] points = new Point[] {A, B, C, D, E};
        PointsCloserThanPredicate predicate = new PointsCloserThanPredicate(13.4);

        TableIterator iterator = new TableIterator(points);
        FilteredIterator filteredIterator = new FilteredIterator(iterator, predicate);

        for (filteredIterator.first(); !filteredIterator.isDone(); filteredIterator.next()) System.out.println(filteredIterator.current());
    }
}