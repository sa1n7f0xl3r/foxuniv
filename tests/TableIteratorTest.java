package tests;

import util.TableIterator;

public class TableIteratorTest
{
    private static class Point
    {
        private final double x;
        private final double y;

        public Point(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        public String toString()
        {
            return String.format("(%.3f, %.3f)", x, y);
        }
    }

    public static void main(String[] args)
    {
        Point A = new Point(2.5, 3.4);
        Point B = new Point(5.3, 3.1);
        Point C = new Point(2.4, 10.2);
        Point D = new Point(2.1, 3.2);
        Point E = new Point(8.6, 1.3);

        Point[] points = new Point[] {A, B, C, D, E};

        TableIterator iterator = new TableIterator(points);

        for (iterator.first(); !iterator.isDone(); iterator.next()) System.out.println(iterator.current());
    }
}