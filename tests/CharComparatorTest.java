package tests;

import util.CharComparator;

public class CharComparatorTest
{
    private static boolean isEqual(int first, int second)
    {
        return first == second;
    }

    public static void main(String[] args)
    {
        CharComparator comparator = new CharComparator();

        System.out.println(isEqual(comparator.compare('c', 'd'), -1));
        System.out.println(isEqual(comparator.compare('d', 'c'), 1));
        System.out.println(isEqual(comparator.compare('c', 'c'), 0));
    }
}