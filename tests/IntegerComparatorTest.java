package tests;

import util.IntegerComparator;

public class IntegerComparatorTest
{
    private static boolean isEqual(int first, int second)
    {
        return first == second;
    }

    public static void main(String[] args)
    {
        IntegerComparator comparator = new IntegerComparator();

        System.out.println(isEqual(comparator.compare(3, 4), -1));
        System.out.println(isEqual(comparator.compare(4, 3), 1));
        System.out.println(isEqual(comparator.compare(4, 4), 0));
    }
}