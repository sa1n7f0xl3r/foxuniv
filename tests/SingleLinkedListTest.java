package tests;

import util.SingleLinkedList;

public class SingleLinkedListTest
{
    public static void main(String[] args)
    {
        SingleLinkedList list = new SingleLinkedList();

        list.add(4);
        list.add(6);
        list.add("Test");
        list.add(32.2);

        System.out.println(list.get(3));

        list.isEmpty();

        list.remove(0);

        list.remove(2);

        list.clear();
    }
}