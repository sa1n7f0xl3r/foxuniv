package util;

import util.interfaces.Iterator;

public class SingleLinkedListIterator implements Iterator
{
    private final SingleLinkedList list;
    private SingleLinkedList.Node current;

    public SingleLinkedListIterator(SingleLinkedList list)
    {
        this.list = list;
        this.current = null;
    }

    public void next() {
        current = current.next;
    }

    public void previous() {
        SingleLinkedList.Node cur = current;
        SingleLinkedList.Node temp = null;

        if (cur == list.getFirst() || cur == null) current = null;
        else if (cur != list.getFirst()) {
            int i = 0;
            current = list.getFirst();
            while (i < list.size())
            {
                if (cur == current)
                {
                    current = temp;
                    break;
                } else {
                    temp = current;
                    current = current.next;
                }

                i++;
            }
        }
    }

    public void first() {
        current = list.getFirst();
    }

    public void last() {
        current = list.getLast();
    }

    public boolean isDone() {
        return current == null;
    }

    public Object current() {
        return current.object;
    }
}