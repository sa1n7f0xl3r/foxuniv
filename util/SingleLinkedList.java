package util;

import util.interfaces.List;

public class SingleLinkedList implements List
{
    protected static class Node
    {
        protected Object object;
        protected Node next;

        private Node(Object object)
        {
            this.object = object;
            this.next = null;
        }
    }

    private int size;
    private Node first;
    private Node last;

    public SingleLinkedList()
    {
        clear();
    }

    public int size() {
        return size;
    }

    public void clear() {
        first = last = null;
        size = 0;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public void add(Object object) {
        Node node = new Node(object);

        if (isEmpty())
        {
            first = last = node;
        } else {
            last.next = node;
            last = node;
        }

        size++;
    }

    public void insert(Object object, int index) throws IndexOutOfBoundsException {
        if (index < 0 || index > size) throw new IndexOutOfBoundsException();

        Node node = new Node(object);
        Node temp = first;

        if (index == 0)
        {
            first = node;
            first.next = temp;
        } else if (index == (size - 1)) {
            last.next = last = node;
        } else if (index < size) {
            Node atIndex = null;
            Node L, L1;

            for (int i = 0; i < size; i++)
            {
                if (i == index)
                {
                    L = atIndex;
                    L1 = L.next;
                    L.next = node;
                    node.next = L1;
                } else {
                    atIndex = temp.next;
                }
            }
        }

        size++;
    }

    public void set(Object object, int index) throws IndexOutOfBoundsException {
        if (index < 0 || index > size) throw new IndexOutOfBoundsException();

        Node temp = first;

        for (int i = 0; i < index; i ++) temp = temp.next;

        temp.object = object;
    }

    public Object get(int index) throws IndexOutOfBoundsException {
        if (index < 0 || index > size) throw new IndexOutOfBoundsException();

        Node temp = first;

        for (int i = 0; i < index; i++) temp = temp.next;

        return temp.object;
    }

    public Object remove(int index) throws IndexOutOfBoundsException {
        if (index < 0 || index > size) throw new IndexOutOfBoundsException();

        Node deleted;

        if (index == 0)
        {
            deleted = first;
            first = first.next;
            if (first == null) last = null;
        } else {
            Node temp = first;

            int i = 1;

            while (i < index)
            {
                temp = temp.next;
                i++;
            }

            deleted = temp.next;
            temp.next = temp.next.next;

            if (temp.next == null) last = temp;
        }

        size--;

        return deleted.object;
    }

    public boolean contains(Object object) {
        Node temp = first;

        for (int i = 0; i < size; i++)
        {
            if (temp.object.equals(object)) return true;
            else {
                temp = temp.next;
                if (temp == null) return false;
            }
        }

        return false;
    }

    public Node getFirst() {
        return first;
    }

    public Node getLast() {
        return last;
    }
}