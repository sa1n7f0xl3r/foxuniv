package util;

import util.interfaces.Comparator;

public class StringComparator implements Comparator
{
    private char[] stringToCharArray(String string)
    {
        char[] result = new char[string.length()];

        for (int i = 0; i < string.length(); i++) result[i] = string.charAt(i);

        return result;
    }

    private int calculateAllChars(String string)
    {
        char[] chars = stringToCharArray(string);
        int summary = 0;

        for (char i: chars) summary += Character.getNumericValue(i);

        return summary;
    }

    public int compare(Object left, Object right) throws ClassCastException
    {
        int l = calculateAllChars((String)left);
        int r = calculateAllChars((String)right);

        return (l - r) < 0 ? -1 : (l - r) > 0 ? 1 : 0;
    }
}