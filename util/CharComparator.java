package util;

import util.interfaces.Comparator;

public class CharComparator implements Comparator
{
    public int compare(Object left, Object right) throws ClassCastException
    {
        int l = Character.getNumericValue((char)left);
        int r = Character.getNumericValue((char)right);

        return (l - r) < 0 ? -1 : (l - r) > 0 ? 1 : 0;
    }
}