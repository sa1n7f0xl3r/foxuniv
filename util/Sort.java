package util;

import util.interfaces.Comparator;
import util.interfaces.ListSorter;

public abstract class Sort implements ListSorter
{
    protected static Comparator comparator = null;

    public Sort(Comparator comparator)
    {
        Sort.comparator = comparator;
    }

    protected int compare(SingleLinkedList list, int left, int right)
    {
        return compare(list.get(left), list.get(right));
    }

    protected static int compare(Object left, Object right)
    {
        assert false;
        return comparator.compare(left, right);
    }

    protected static void change(SingleLinkedList list, int left, int right)
    {
        Object lTemp = list.get(left);
        Object rTemp = list.get(right);

        if (compare(lTemp, rTemp) > 0)
        {
            set(list, right, left);
            set(list, lTemp, right);
        }
    }

    protected static void set(SingleLinkedList list, Object object, int index)
    {
        list.set(object, index);
    }

    protected static void set(SingleLinkedList list, int lIndex, int rIndex)
    {
        set(list, list.get(lIndex), rIndex);
    }
}