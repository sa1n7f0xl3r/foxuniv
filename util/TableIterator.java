package util;

import util.interfaces.Iterator;

public class TableIterator implements Iterator
{
    private final Object[] objects;
    private final int first;
    private final int last;
    private int current = -1;

    public TableIterator(Object[] objects, int from, int to)
    {
        this.objects = objects;
        this.first = from;
        this.last = from + to - 1;
    }

    public TableIterator(Object[] objects)
    {
        this.objects = objects;
        this.first = 0;
        this.last = this.objects.length - 1;
    }

    public void next() {
        ++current;
    }

    public void previous() {
        --current;
    }

    public void first() {
        current = first;
    }

    public void last() {
        current = last;
    }

    public boolean isDone() {
        return current < first || current > last;
    }

    public Object current() {
        return objects[current];
    }
}