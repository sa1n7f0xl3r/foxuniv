package util;

import util.interfaces.Comparator;

public class IntegerComparator implements Comparator
{
    public int compare(Object left, Object right) throws ClassCastException
    {
        Integer l = (Integer)left;
        Integer r = (Integer)right;

        return (l - r) < 0 ? -1 : (l - r) > 0 ? 1 : 0;
    }
}