package util;

import util.interfaces.Comparator;

public class DoubleComparator implements Comparator
{
    public int compare(Object left, Object right) throws ClassCastException
    {
        Double l = (Double)left;
        Double r = (Double)right;

        return (l - r) < 0 ? -1 : (l - r) > 0 ? 1 : 0;
    }
}