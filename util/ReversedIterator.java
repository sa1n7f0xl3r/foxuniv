package util;

import util.interfaces.Iterator;

public class ReversedIterator implements Iterator
{
    private final Iterator iterator;

    public ReversedIterator(Iterator iterator)
    {
        this.iterator = iterator;
    }

    public void next()
    {
        this.iterator.previous();
    }

    public void previous()
    {
        this.iterator.next();
    }

    public void first()
    {
        this.iterator.last();
    }

    public void last()
    {
        this.iterator.first();
    }

    public boolean isDone()
    {
        return this.iterator.isDone();
    }

    public Object current()
    {
        return this.iterator.current();
    }
}