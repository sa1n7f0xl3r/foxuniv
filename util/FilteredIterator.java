package util;

import util.interfaces.Iterator;
import util.interfaces.Predicate;

public class FilteredIterator  implements Iterator
{
    private final Iterator iterator;
    private final Predicate predicate;

    public FilteredIterator(Iterator iterator, Predicate predicate)
    {
        this.iterator = iterator;
        this.predicate = predicate;
        this.iterator.first();
    }

    private void forwardFiltered()
    {
        while (!this.iterator.isDone() && !this.predicate.accept(this.iterator.current())) this.iterator.next();
    }

    private void backwardFiltered()
    {
        while (!this.iterator.isDone() && !this.predicate.accept(this.iterator.current())) this.iterator.previous();
    }

    public void next()
    {
        this.iterator.next();
        forwardFiltered();;
    }

    public void previous()
    {
        this.iterator.previous();
    }

    public void first()
    {
        this.iterator.first();
        forwardFiltered();
    }

    public void last()
    {
        this.iterator.last();
        backwardFiltered();
    }

    public boolean isDone()
    {
        return this.iterator.isDone();
    }

    public Object current()
    {
        return this.iterator.current();
    }
}