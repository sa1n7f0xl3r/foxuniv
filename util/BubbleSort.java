package util;

import util.interfaces.Comparator;

public class BubbleSort extends Sort
{
    public BubbleSort(Comparator comparator) {
        super(comparator);
    }

    public SingleLinkedList sort(SingleLinkedList list) {
        for (int i = 0; i < list.size(); i++)
        {
            for (int j = i + 1; j < list.size(); j++)
            {
                if (comparator.compare(list.get(i), list.get(j)) > 0) change(list, i, j);
            }
        }

        return list;
    }
}