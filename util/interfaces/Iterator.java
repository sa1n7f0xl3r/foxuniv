package util.interfaces;

public interface Iterator
{
    public void next();
    public void previous();

    public void first();
    public void last();

    public boolean isDone();

    public Object current();
}