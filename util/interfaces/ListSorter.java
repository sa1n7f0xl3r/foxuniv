package util.interfaces;

import util.SingleLinkedList;

public interface ListSorter
{
    public SingleLinkedList sort(SingleLinkedList list);
}