package util.interfaces;

public interface Comparator
{
    public int compare(Object left, Object right) throws ClassCastException;
}