package util.interfaces;

public interface Predicate
{
    public boolean accept(Object object);
}